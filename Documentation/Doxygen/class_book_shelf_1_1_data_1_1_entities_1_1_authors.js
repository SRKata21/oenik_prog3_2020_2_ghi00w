var class_book_shelf_1_1_data_1_1_entities_1_1_authors =
[
    [ "Authors", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#aa751f1a9e1eacd2603589e27b3a09235", null ],
    [ "Equals", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a8a64f6e3ca00a6168b141089748113ff", null ],
    [ "GetHashCode", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#ab9111b9f5f1612444b3ac8e12cbe0bc9", null ],
    [ "ToString", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a9320887046004b6e12deaa0d9496846f", null ],
    [ "AuthorId", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a38c34a41fda764c093c7705236e93e42", null ],
    [ "BirthCountry", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a0bf171d87e03a5d789a450d8ba4ddb5c", null ],
    [ "BirthName", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a7558e274ec063414cc20df8137f43838", null ],
    [ "BirthYear", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#afeaeabba5fa9a5c8b0f3c65a68cd8676", null ],
    [ "Bks", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#aadd70232fe9026f5a475f8a674a6a518", null ],
    [ "Name", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a27fa1fa3e986fb713f948862e370be0d", null ],
    [ "Nationality", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a39d40180ba7507c4630bf388972f70ea", null ]
];