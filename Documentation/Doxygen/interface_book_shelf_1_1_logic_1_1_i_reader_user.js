var interface_book_shelf_1_1_logic_1_1_i_reader_user =
[
    [ "ChangePersonAddress", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html#a6abe67f53f01a726a7df8eb919a2c4e9", null ],
    [ "ChangePersonEmail", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html#a161bf3ec8ec78f4dbf59f0ea8a2828b4", null ],
    [ "GetAllAuthors", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html#a5315c3e013e5ee1937c8c67c2dac0b88", null ],
    [ "GetAllBooks", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html#a2026c00a67ab3c4a66060d6543d00f66", null ],
    [ "GetBookHungarian", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html#ab613d58fd06f8901b1d33864746ba035", null ],
    [ "GetFamousBooks", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html#af5e29a80bee30b29db4961d14912f85b", null ]
];