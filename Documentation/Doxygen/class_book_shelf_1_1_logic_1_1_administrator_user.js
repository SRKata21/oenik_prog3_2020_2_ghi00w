var class_book_shelf_1_1_logic_1_1_administrator_user =
[
    [ "AdministratorUser", "class_book_shelf_1_1_logic_1_1_administrator_user.html#ad5c553b081b0cb1a9c312e7fc74b2bbe", null ],
    [ "GetAllPeople", "class_book_shelf_1_1_logic_1_1_administrator_user.html#a43aa7ede33e1c0140ba4a9979ef733cb", null ],
    [ "GetOnePerson", "class_book_shelf_1_1_logic_1_1_administrator_user.html#a56e8263a251ae69e9a6ac1aeac325abb", null ],
    [ "InsertAuthor", "class_book_shelf_1_1_logic_1_1_administrator_user.html#a88ba6b6356a7c15a889ed6392102c9a3", null ],
    [ "InsertBook", "class_book_shelf_1_1_logic_1_1_administrator_user.html#af9dacb2f465b6bc53cbf1d9eb735f882", null ],
    [ "InsertPerson", "class_book_shelf_1_1_logic_1_1_administrator_user.html#a56f949fde1fa2707532f7a197d238cce", null ],
    [ "RemoveAuthorByKey", "class_book_shelf_1_1_logic_1_1_administrator_user.html#a36c76aba3b34bdcb0f6cf18d935a5827", null ],
    [ "RemoveBookByKey", "class_book_shelf_1_1_logic_1_1_administrator_user.html#aa6b0608b68ce1a2736321ebc5f62ce00", null ],
    [ "RemovePersonByKey", "class_book_shelf_1_1_logic_1_1_administrator_user.html#a7fdee9314ab582019935cc10833bf9b2", null ]
];