var class_book_shelf_1_1_data_1_1_entities_1_1_lends =
[
    [ "Equals", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a9eb0dc3897d5eb908c91e3235ba982c0", null ],
    [ "GetHashCode", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a3dd8585e9f8fc472f54328191b9b190b", null ],
    [ "ToString", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a43f3a549a8d1957b831d215be438d89a", null ],
    [ "Bok", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a75f2ef5fd919b6a03db82fe50b18e8b2", null ],
    [ "BookId", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a57b313af27ce0994eb2f423cdb0b2424", null ],
    [ "GivebackDate", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#af512a46e7bb757d6acc1e777aa3b2bd3", null ],
    [ "LendDate", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a3c74018be74c92bea73414378d99af4d", null ],
    [ "LendId", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#af4cb7869c15518a36883ea99da39157d", null ],
    [ "PersonId", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a663aae57a0cc946b897e344e41f0330b", null ],
    [ "Pple", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#aab251c4852ef7e4bf10b6a64ef6bd9b6", null ]
];