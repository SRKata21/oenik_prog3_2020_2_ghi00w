var hierarchy =
[
    [ "BookShelf.Logic.Tests.AdministratorTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_administrator_test.html", null ],
    [ "BookShelf.Data.Entities.Authors", "class_book_shelf_1_1_data_1_1_entities_1_1_authors.html", null ],
    [ "BookShelf.Logic.BookDatas", "class_book_shelf_1_1_logic_1_1_book_datas.html", null ],
    [ "BookShelf.Data.Entities.Books", "class_book_shelf_1_1_data_1_1_entities_1_1_books.html", null ],
    [ "BookShelf.Program.ConsoleCommunicator", "class_book_shelf_1_1_program_1_1_console_communicator.html", null ],
    [ "DbContext", null, [
      [ "BookShelf.Data.Entities.BookShelfDBContext", "class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html", null ]
    ] ],
    [ "BookShelf.Program.Factory", "class_book_shelf_1_1_program_1_1_factory.html", null ],
    [ "BookShelf.Logic.IAdministratorUser", "interface_book_shelf_1_1_logic_1_1_i_administrator_user.html", [
      [ "BookShelf.Logic.AdministratorUser", "class_book_shelf_1_1_logic_1_1_administrator_user.html", null ]
    ] ],
    [ "BookShelf.Logic.ILibrarianUser", "interface_book_shelf_1_1_logic_1_1_i_librarian_user.html", [
      [ "BookShelf.Logic.LibrarianUser", "class_book_shelf_1_1_logic_1_1_librarian_user.html", null ]
    ] ],
    [ "BookShelf.Repository.IManager< T >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.Manager< T >", "class_book_shelf_1_1_repository_1_1_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.IManager< Authors >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.IAuthorManager", "interface_book_shelf_1_1_repository_1_1_i_author_manager.html", [
        [ "BookShelf.Repository.AuthorManager", "class_book_shelf_1_1_repository_1_1_author_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Repository.IManager< Books >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.IBookManager", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html", [
        [ "BookShelf.Repository.BookManager", "class_book_shelf_1_1_repository_1_1_book_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Repository.IManager< Lends >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.ILendManager", "interface_book_shelf_1_1_repository_1_1_i_lend_manager.html", [
        [ "BookShelf.Repository.LendManager", "class_book_shelf_1_1_repository_1_1_lend_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Repository.IManager< People >", "interface_book_shelf_1_1_repository_1_1_i_manager.html", [
      [ "BookShelf.Repository.IPeopleManager", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html", [
        [ "BookShelf.Repository.PeopleManager", "class_book_shelf_1_1_repository_1_1_people_manager.html", null ]
      ] ]
    ] ],
    [ "BookShelf.Logic.IReaderUser", "interface_book_shelf_1_1_logic_1_1_i_reader_user.html", [
      [ "BookShelf.Logic.ReaderUser", "class_book_shelf_1_1_logic_1_1_reader_user.html", null ]
    ] ],
    [ "BookShelf.Logic.LendingByReader", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html", null ],
    [ "BookShelf.Data.Entities.Lends", "class_book_shelf_1_1_data_1_1_entities_1_1_lends.html", null ],
    [ "BookShelf.Logic.Tests.LibrarianTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_librarian_test.html", null ],
    [ "BookShelf.Logic.Tests.LogicCreator", "class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html", null ],
    [ "BookShelf.Repository.Manager< Authors >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.AuthorManager", "class_book_shelf_1_1_repository_1_1_author_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.Manager< Books >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.BookManager", "class_book_shelf_1_1_repository_1_1_book_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.Manager< Lends >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.LendManager", "class_book_shelf_1_1_repository_1_1_lend_manager.html", null ]
    ] ],
    [ "BookShelf.Repository.Manager< People >", "class_book_shelf_1_1_repository_1_1_manager.html", [
      [ "BookShelf.Repository.PeopleManager", "class_book_shelf_1_1_repository_1_1_people_manager.html", null ]
    ] ],
    [ "BookShelf.Data.Entities.People", "class_book_shelf_1_1_data_1_1_entities_1_1_people.html", null ],
    [ "BookShelf.Logic.Tests.ReaderTest", "class_book_shelf_1_1_logic_1_1_tests_1_1_reader_test.html", null ]
];