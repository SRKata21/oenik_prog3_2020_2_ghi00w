var class_book_shelf_1_1_logic_1_1_lending_by_reader =
[
    [ "Equals", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a6c1b5c9b8e2af0a2d12a7928cd01b1b9", null ],
    [ "GetHashCode", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#aa32f6d50c25ead4b32636810fd549267", null ],
    [ "ToString", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a1e10b3d95e36a9af2fbd53ebcfe850ba", null ],
    [ "AuthorName", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a21548597e4304b7d22ebd06153a936d5", null ],
    [ "GiveBackDate", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a460496b28cf7d9ef4be0994b39e0008b", null ],
    [ "LendDate", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a21329fae721424166f9a49d91f6c6191", null ],
    [ "ReaderName", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a97fabbc84931fed13cb7483c2d42af36", null ],
    [ "Title", "class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a0800acaafd911332fb1883a3317d0e1a", null ]
];