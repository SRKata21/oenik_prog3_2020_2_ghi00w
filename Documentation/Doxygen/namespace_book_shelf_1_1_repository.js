var namespace_book_shelf_1_1_repository =
[
    [ "AuthorManager", "class_book_shelf_1_1_repository_1_1_author_manager.html", "class_book_shelf_1_1_repository_1_1_author_manager" ],
    [ "BookManager", "class_book_shelf_1_1_repository_1_1_book_manager.html", "class_book_shelf_1_1_repository_1_1_book_manager" ],
    [ "IAuthorManager", "interface_book_shelf_1_1_repository_1_1_i_author_manager.html", "interface_book_shelf_1_1_repository_1_1_i_author_manager" ],
    [ "IBookManager", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html", "interface_book_shelf_1_1_repository_1_1_i_book_manager" ],
    [ "ILendManager", "interface_book_shelf_1_1_repository_1_1_i_lend_manager.html", "interface_book_shelf_1_1_repository_1_1_i_lend_manager" ],
    [ "IManager", "interface_book_shelf_1_1_repository_1_1_i_manager.html", "interface_book_shelf_1_1_repository_1_1_i_manager" ],
    [ "IPeopleManager", "interface_book_shelf_1_1_repository_1_1_i_people_manager.html", "interface_book_shelf_1_1_repository_1_1_i_people_manager" ],
    [ "LendManager", "class_book_shelf_1_1_repository_1_1_lend_manager.html", "class_book_shelf_1_1_repository_1_1_lend_manager" ],
    [ "Manager", "class_book_shelf_1_1_repository_1_1_manager.html", "class_book_shelf_1_1_repository_1_1_manager" ],
    [ "PeopleManager", "class_book_shelf_1_1_repository_1_1_people_manager.html", "class_book_shelf_1_1_repository_1_1_people_manager" ]
];