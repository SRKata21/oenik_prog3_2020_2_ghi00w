var class_book_shelf_1_1_repository_1_1_book_manager =
[
    [ "BookManager", "class_book_shelf_1_1_repository_1_1_book_manager.html#ac8724acc8199c44e1a634d9a233d8b3f", null ],
    [ "ChangeAuthor", "class_book_shelf_1_1_repository_1_1_book_manager.html#a8557b69b1c8e4654f96bd905a4b19ebc", null ],
    [ "ChangeGetDate", "class_book_shelf_1_1_repository_1_1_book_manager.html#a82bd5e262ece39bacef78c43aa0c38db", null ],
    [ "ChangePage", "class_book_shelf_1_1_repository_1_1_book_manager.html#a7ee04af5b095af0cca137df693e473d9", null ],
    [ "ChangePlace", "class_book_shelf_1_1_repository_1_1_book_manager.html#ab9b0153c62c7eff83c537ffff31c4d49", null ],
    [ "ChangePublisher", "class_book_shelf_1_1_repository_1_1_book_manager.html#a5d2aa31208060a01b956888d9db1a650", null ],
    [ "ChangeTitle", "class_book_shelf_1_1_repository_1_1_book_manager.html#acb924229756a452120b7d141bf4d124d", null ],
    [ "ChangeYear", "class_book_shelf_1_1_repository_1_1_book_manager.html#a4684419825b6f5c0d6513a77bdc3795c", null ],
    [ "GetOne", "class_book_shelf_1_1_repository_1_1_book_manager.html#ab2b2449989c99072de660425378e17f2", null ]
];