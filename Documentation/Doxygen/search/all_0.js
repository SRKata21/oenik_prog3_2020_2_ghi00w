var searchData=
[
  ['address_0',['Address',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#abd0cde8e3f75f2ffacd0109bc66ece7f',1,'BookShelf::Data::Entities::People']]],
  ['adm_1',['Adm',['../class_book_shelf_1_1_program_1_1_factory.html#ae573eb5f595de87f518442ae8c8e5a13',1,'BookShelf::Program::Factory']]],
  ['administratortest_2',['AdministratorTest',['../class_book_shelf_1_1_logic_1_1_tests_1_1_administrator_test.html',1,'BookShelf::Logic::Tests']]],
  ['administratoruser_3',['AdministratorUser',['../class_book_shelf_1_1_logic_1_1_administrator_user.html',1,'BookShelf.Logic.AdministratorUser'],['../class_book_shelf_1_1_logic_1_1_administrator_user.html#ad5c553b081b0cb1a9c312e7fc74b2bbe',1,'BookShelf.Logic.AdministratorUser.AdministratorUser()']]],
  ['authorid_4',['AuthorId',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a38c34a41fda764c093c7705236e93e42',1,'BookShelf.Data.Entities.Authors.AuthorId()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#af3a832483ff9eb6224c1a7897e2d46fe',1,'BookShelf.Data.Entities.Books.AuthorId()']]],
  ['authormanager_5',['AuthorManager',['../class_book_shelf_1_1_repository_1_1_author_manager.html',1,'BookShelf.Repository.AuthorManager'],['../class_book_shelf_1_1_repository_1_1_author_manager.html#ab24ab09db9ee25aee907a995d85d5c7a',1,'BookShelf.Repository.AuthorManager.AuthorManager()']]],
  ['authorname_6',['AuthorName',['../class_book_shelf_1_1_logic_1_1_book_datas.html#a3bd815079749f2c7dff82ac0267ebb3e',1,'BookShelf.Logic.BookDatas.AuthorName()'],['../class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a21548597e4304b7d22ebd06153a936d5',1,'BookShelf.Logic.LendingByReader.AuthorName()']]],
  ['authors_7',['Authors',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html',1,'BookShelf.Data.Entities.Authors'],['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a3485c6552909c27645ee8b04c63ad89c',1,'BookShelf.Data.Entities.BookShelfDBContext.Authors()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#aa751f1a9e1eacd2603589e27b3a09235',1,'BookShelf.Data.Entities.Authors.Authors()']]],
  ['authr_8',['Authr',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#adbdbda180f7c4ccf4d1b89ccfb42be31',1,'BookShelf::Data::Entities::Books']]]
];
