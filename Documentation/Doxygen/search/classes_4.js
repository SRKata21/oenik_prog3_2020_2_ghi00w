var searchData=
[
  ['iadministratoruser_162',['IAdministratorUser',['../interface_book_shelf_1_1_logic_1_1_i_administrator_user.html',1,'BookShelf::Logic']]],
  ['iauthormanager_163',['IAuthorManager',['../interface_book_shelf_1_1_repository_1_1_i_author_manager.html',1,'BookShelf::Repository']]],
  ['ibookmanager_164',['IBookManager',['../interface_book_shelf_1_1_repository_1_1_i_book_manager.html',1,'BookShelf::Repository']]],
  ['ilendmanager_165',['ILendManager',['../interface_book_shelf_1_1_repository_1_1_i_lend_manager.html',1,'BookShelf::Repository']]],
  ['ilibrarianuser_166',['ILibrarianUser',['../interface_book_shelf_1_1_logic_1_1_i_librarian_user.html',1,'BookShelf::Logic']]],
  ['imanager_167',['IManager',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20authors_20_3e_168',['IManager&lt; Authors &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20books_20_3e_169',['IManager&lt; Books &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20lends_20_3e_170',['IManager&lt; Lends &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20people_20_3e_171',['IManager&lt; People &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['ipeoplemanager_172',['IPeopleManager',['../interface_book_shelf_1_1_repository_1_1_i_people_manager.html',1,'BookShelf::Repository']]],
  ['ireaderuser_173',['IReaderUser',['../interface_book_shelf_1_1_logic_1_1_i_reader_user.html',1,'BookShelf::Logic']]]
];
