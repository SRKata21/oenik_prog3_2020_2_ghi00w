var searchData=
[
  ['bookshelf_189',['BookShelf',['../namespace_book_shelf.html',1,'']]],
  ['data_190',['Data',['../namespace_book_shelf_1_1_data.html',1,'BookShelf']]],
  ['entities_191',['Entities',['../namespace_book_shelf_1_1_data_1_1_entities.html',1,'BookShelf::Data']]],
  ['logic_192',['Logic',['../namespace_book_shelf_1_1_logic.html',1,'BookShelf']]],
  ['program_193',['Program',['../namespace_book_shelf_1_1_program.html',1,'BookShelf']]],
  ['repository_194',['Repository',['../namespace_book_shelf_1_1_repository.html',1,'BookShelf']]],
  ['tests_195',['Tests',['../namespace_book_shelf_1_1_logic_1_1_tests.html',1,'BookShelf::Logic']]]
];
