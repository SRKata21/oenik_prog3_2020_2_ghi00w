var searchData=
[
  ['address_276',['Address',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#abd0cde8e3f75f2ffacd0109bc66ece7f',1,'BookShelf::Data::Entities::People']]],
  ['adm_277',['Adm',['../class_book_shelf_1_1_program_1_1_factory.html#ae573eb5f595de87f518442ae8c8e5a13',1,'BookShelf::Program::Factory']]],
  ['authorid_278',['AuthorId',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a38c34a41fda764c093c7705236e93e42',1,'BookShelf.Data.Entities.Authors.AuthorId()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#af3a832483ff9eb6224c1a7897e2d46fe',1,'BookShelf.Data.Entities.Books.AuthorId()']]],
  ['authorname_279',['AuthorName',['../class_book_shelf_1_1_logic_1_1_book_datas.html#a3bd815079749f2c7dff82ac0267ebb3e',1,'BookShelf.Logic.BookDatas.AuthorName()'],['../class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a21548597e4304b7d22ebd06153a936d5',1,'BookShelf.Logic.LendingByReader.AuthorName()']]],
  ['authors_280',['Authors',['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a3485c6552909c27645ee8b04c63ad89c',1,'BookShelf::Data::Entities::BookShelfDBContext']]],
  ['authr_281',['Authr',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#adbdbda180f7c4ccf4d1b89ccfb42be31',1,'BookShelf::Data::Entities::Books']]]
];
