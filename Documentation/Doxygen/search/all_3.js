var searchData=
[
  ['email_58',['Email',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#aff327141de52a457422e88e77e9acce6',1,'BookShelf::Data::Entities::People']]],
  ['enterdate_59',['EnterDate',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a781976f763e74e4ca2189caba7d9b3b2',1,'BookShelf::Data::Entities::People']]],
  ['equals_60',['Equals',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a8a64f6e3ca00a6168b141089748113ff',1,'BookShelf.Data.Entities.Authors.Equals()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a2ec3c498528804df8ab7b1922cb11a57',1,'BookShelf.Data.Entities.Books.Equals()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a9eb0dc3897d5eb908c91e3235ba982c0',1,'BookShelf.Data.Entities.Lends.Equals()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a2a4af50f710ae201632316423ffd6c85',1,'BookShelf.Data.Entities.People.Equals()'],['../class_book_shelf_1_1_logic_1_1_book_datas.html#ac09378b36353ccc033efeaebb13c6090',1,'BookShelf.Logic.BookDatas.Equals()'],['../class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a6c1b5c9b8e2af0a2d12a7928cd01b1b9',1,'BookShelf.Logic.LendingByReader.Equals()']]],
  ['expectedfambooks_61',['ExpectedFamBooks',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a770b6af2450214e9b935300d8fcc6434',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['expectedhunbooks_62',['ExpectedHunBooks',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#ad94e6e061063da93a6310d3888e316e1',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['expectedlends_63',['ExpectedLends',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a89ce9757c74aeaf22ae79f2690c2c5c5',1,'BookShelf::Logic::Tests::LogicCreator']]]
];
