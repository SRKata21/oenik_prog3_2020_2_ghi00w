var searchData=
[
  ['lbr_299',['Lbr',['../class_book_shelf_1_1_program_1_1_factory.html#ac8795fdad7ecbda1cbe80eaa4da6b0d8',1,'BookShelf::Program::Factory']]],
  ['lenddate_300',['LendDate',['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a3c74018be74c92bea73414378d99af4d',1,'BookShelf.Data.Entities.Lends.LendDate()'],['../class_book_shelf_1_1_logic_1_1_lending_by_reader.html#a21329fae721424166f9a49d91f6c6191',1,'BookShelf.Logic.LendingByReader.LendDate()']]],
  ['lendid_301',['LendId',['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#af4cb7869c15518a36883ea99da39157d',1,'BookShelf::Data::Entities::Lends']]],
  ['lends_302',['Lends',['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#ab4b250d3e4497f9093b9eba9b25851c6',1,'BookShelf::Data::Entities::BookShelfDBContext']]],
  ['lnd_303',['Lnd',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a445c07bef92caff12d1a16fa1ef5e9a2',1,'BookShelf.Data.Entities.Books.Lnd()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a90419befaf4ad068b88643ea983b3c42',1,'BookShelf.Data.Entities.People.Lnd()']]]
];
