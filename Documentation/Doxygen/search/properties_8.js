var searchData=
[
  ['pages_311',['Pages',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a88ca864251efc53975ae81cba7daccc7',1,'BookShelf::Data::Entities::Books']]],
  ['people_312',['People',['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a910dafb6aa24e9e241a6d2bccf677b1a',1,'BookShelf::Data::Entities::BookShelfDBContext']]],
  ['personid_313',['PersonId',['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a663aae57a0cc946b897e344e41f0330b',1,'BookShelf.Data.Entities.Lends.PersonId()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a78ead423b82bb24d2394c2a87a179771',1,'BookShelf.Data.Entities.People.PersonId()']]],
  ['place_314',['Place',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#a971f6cfe79108c0edcd63ac13b26fe24',1,'BookShelf::Data::Entities::Books']]],
  ['pple_315',['Pple',['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#aab251c4852ef7e4bf10b6a64ef6bd9b6',1,'BookShelf::Data::Entities::Lends']]],
  ['publisher_316',['Publisher',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#af2df60194945cb0f839c4d173cec5a69',1,'BookShelf::Data::Entities::Books']]]
];
