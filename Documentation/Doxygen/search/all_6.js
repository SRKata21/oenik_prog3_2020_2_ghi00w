var searchData=
[
  ['iadministratoruser_81',['IAdministratorUser',['../interface_book_shelf_1_1_logic_1_1_i_administrator_user.html',1,'BookShelf::Logic']]],
  ['iauthormanager_82',['IAuthorManager',['../interface_book_shelf_1_1_repository_1_1_i_author_manager.html',1,'BookShelf::Repository']]],
  ['ibookmanager_83',['IBookManager',['../interface_book_shelf_1_1_repository_1_1_i_book_manager.html',1,'BookShelf::Repository']]],
  ['id_84',['Id',['../class_book_shelf_1_1_logic_1_1_book_datas.html#a4731b486e7d8e43bf9e9727d4b4d35f4',1,'BookShelf::Logic::BookDatas']]],
  ['ilendmanager_85',['ILendManager',['../interface_book_shelf_1_1_repository_1_1_i_lend_manager.html',1,'BookShelf::Repository']]],
  ['ilibrarianuser_86',['ILibrarianUser',['../interface_book_shelf_1_1_logic_1_1_i_librarian_user.html',1,'BookShelf::Logic']]],
  ['imanager_87',['IManager',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20authors_20_3e_88',['IManager&lt; Authors &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20books_20_3e_89',['IManager&lt; Books &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20lends_20_3e_90',['IManager&lt; Lends &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['imanager_3c_20people_20_3e_91',['IManager&lt; People &gt;',['../interface_book_shelf_1_1_repository_1_1_i_manager.html',1,'BookShelf::Repository']]],
  ['insert_92',['Insert',['../interface_book_shelf_1_1_repository_1_1_i_manager.html#ab1fc09fd8bf2a0e0d1f0424f3b4f5276',1,'BookShelf.Repository.IManager.Insert()'],['../class_book_shelf_1_1_repository_1_1_manager.html#a6038a9b5d3a68d2cbc911da67d92d24d',1,'BookShelf.Repository.Manager.Insert()']]],
  ['insertauthor_93',['InsertAuthor',['../class_book_shelf_1_1_logic_1_1_administrator_user.html#a88ba6b6356a7c15a889ed6392102c9a3',1,'BookShelf.Logic.AdministratorUser.InsertAuthor()'],['../interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#af37c01ec445c4a4bb2180266d8b5d7c3',1,'BookShelf.Logic.IAdministratorUser.InsertAuthor()']]],
  ['insertbook_94',['InsertBook',['../class_book_shelf_1_1_logic_1_1_administrator_user.html#af9dacb2f465b6bc53cbf1d9eb735f882',1,'BookShelf.Logic.AdministratorUser.InsertBook()'],['../interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#a2b5d7c07e543bf52b654a8b0db3ad074',1,'BookShelf.Logic.IAdministratorUser.InsertBook()']]],
  ['insertlend_95',['InsertLend',['../interface_book_shelf_1_1_logic_1_1_i_librarian_user.html#a42c2dcaefb0ecc92759d32cb962ac2e7',1,'BookShelf.Logic.ILibrarianUser.InsertLend()'],['../class_book_shelf_1_1_logic_1_1_librarian_user.html#ab6d61d08a6ab9af09a4ab3b87a46ddf4',1,'BookShelf.Logic.LibrarianUser.InsertLend()']]],
  ['insertperson_96',['InsertPerson',['../class_book_shelf_1_1_logic_1_1_administrator_user.html#a56f949fde1fa2707532f7a197d238cce',1,'BookShelf.Logic.AdministratorUser.InsertPerson()'],['../interface_book_shelf_1_1_logic_1_1_i_administrator_user.html#a974170a2874c8028498a74783affd3ce',1,'BookShelf.Logic.IAdministratorUser.InsertPerson()']]],
  ['ipeoplemanager_97',['IPeopleManager',['../interface_book_shelf_1_1_repository_1_1_i_people_manager.html',1,'BookShelf::Repository']]],
  ['ireaderuser_98',['IReaderUser',['../interface_book_shelf_1_1_logic_1_1_i_reader_user.html',1,'BookShelf::Logic']]]
];
