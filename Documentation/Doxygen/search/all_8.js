var searchData=
[
  ['manager_109',['Manager',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf.Repository.Manager&lt; T &gt;'],['../class_book_shelf_1_1_repository_1_1_manager.html#a1ae5997ebe852fd680c6df5a0196698b',1,'BookShelf.Repository.Manager.Manager()']]],
  ['manager_3c_20authors_20_3e_110',['Manager&lt; Authors &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20books_20_3e_111',['Manager&lt; Books &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20lends_20_3e_112',['Manager&lt; Lends &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['manager_3c_20people_20_3e_113',['Manager&lt; People &gt;',['../class_book_shelf_1_1_repository_1_1_manager.html',1,'BookShelf::Repository']]],
  ['mockauthorrepo_114',['MockAuthorRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#ad6b5c622cb139e42e601633bef77b97b',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mockbookrepo_115',['MockBookRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a47ff680302e2f70dc5b63909bab5a874',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mocklendrepo_116',['MockLendRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#aecba52869f40e99b55bd06531b16a9a9',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mockpeoplerepo_117',['MockPeopleRepo',['../class_book_shelf_1_1_logic_1_1_tests_1_1_logic_creator.html#a32b858e47a7bbf4a7b7b19b47c3ddf15',1,'BookShelf::Logic::Tests::LogicCreator']]],
  ['mothername_118',['MotherName',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#ae59923556086a7df863793f18df3876b',1,'BookShelf::Data::Entities::People']]]
];
