var searchData=
[
  ['birthcountry_282',['BirthCountry',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a0bf171d87e03a5d789a450d8ba4ddb5c',1,'BookShelf::Data::Entities::Authors']]],
  ['birthdate_283',['BirthDate',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#aef58ea514af63c578f1021308175bf9b',1,'BookShelf::Data::Entities::People']]],
  ['birthname_284',['BirthName',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#a7558e274ec063414cc20df8137f43838',1,'BookShelf::Data::Entities::Authors']]],
  ['birthplace_285',['BirthPlace',['../class_book_shelf_1_1_data_1_1_entities_1_1_people.html#a4edd845864cd7c1690e9974e2c443694',1,'BookShelf::Data::Entities::People']]],
  ['birthyear_286',['BirthYear',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#afeaeabba5fa9a5c8b0f3c65a68cd8676',1,'BookShelf::Data::Entities::Authors']]],
  ['bks_287',['Bks',['../class_book_shelf_1_1_data_1_1_entities_1_1_authors.html#aadd70232fe9026f5a475f8a674a6a518',1,'BookShelf::Data::Entities::Authors']]],
  ['bok_288',['Bok',['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a75f2ef5fd919b6a03db82fe50b18e8b2',1,'BookShelf::Data::Entities::Lends']]],
  ['bookid_289',['BookId',['../class_book_shelf_1_1_data_1_1_entities_1_1_books.html#ac57b1bd42cca27798e4375ba5a8131eb',1,'BookShelf.Data.Entities.Books.BookId()'],['../class_book_shelf_1_1_data_1_1_entities_1_1_lends.html#a57b313af27ce0994eb2f423cdb0b2424',1,'BookShelf.Data.Entities.Lends.BookId()']]],
  ['books_290',['Books',['../class_book_shelf_1_1_data_1_1_entities_1_1_book_shelf_d_b_context.html#a1700ac9e5bd8449ae35f01fad66cbb97',1,'BookShelf::Data::Entities::BookShelfDBContext']]]
];
