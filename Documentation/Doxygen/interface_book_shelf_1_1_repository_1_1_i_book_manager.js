var interface_book_shelf_1_1_repository_1_1_i_book_manager =
[
    [ "ChangeAuthor", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#a2cae8d77b634acf0b14b620dc895146f", null ],
    [ "ChangeGetDate", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#ae174a4405c3a595a8ae69e4cf170256e", null ],
    [ "ChangePage", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#aa04865877029bde440f9efb31e8fa096", null ],
    [ "ChangePlace", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#a2559ee8e97926310509e981ee5385cba", null ],
    [ "ChangePublisher", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#afeb9724bcb4163e8017dabaa419a19c5", null ],
    [ "ChangeTitle", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#aa938630109debff9c525a2ddd130ec26", null ],
    [ "ChangeYear", "interface_book_shelf_1_1_repository_1_1_i_book_manager.html#adf766e01881563d77114f26b5d4e551b", null ]
];