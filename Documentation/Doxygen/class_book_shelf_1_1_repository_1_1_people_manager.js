var class_book_shelf_1_1_repository_1_1_people_manager =
[
    [ "PeopleManager", "class_book_shelf_1_1_repository_1_1_people_manager.html#a67a7787871ae5db1e624b4233b68d189", null ],
    [ "ChangeAddress", "class_book_shelf_1_1_repository_1_1_people_manager.html#a9fb160412c9f297f66135e63dfdd9008", null ],
    [ "ChangeBirthDate", "class_book_shelf_1_1_repository_1_1_people_manager.html#a3f5a79841ad55709b75ec7d8731f52b9", null ],
    [ "ChangeBirthPlace", "class_book_shelf_1_1_repository_1_1_people_manager.html#a7484afcec2de423c9550de4dda728a08", null ],
    [ "ChangeEmail", "class_book_shelf_1_1_repository_1_1_people_manager.html#a986e083b33ee9eac73115d507ea068b1", null ],
    [ "ChangeEnterDate", "class_book_shelf_1_1_repository_1_1_people_manager.html#acef4dac8d342baa06950a6463f0e2b8c", null ],
    [ "ChangeMotherName", "class_book_shelf_1_1_repository_1_1_people_manager.html#af8dedfe775324b5f9ab25ea3f967fb75", null ],
    [ "ChangeName", "class_book_shelf_1_1_repository_1_1_people_manager.html#ac2adaf2050f60d6624ea491d87110790", null ],
    [ "GetOne", "class_book_shelf_1_1_repository_1_1_people_manager.html#a7bd7972f235422306445d2abfa3a9cad", null ]
];