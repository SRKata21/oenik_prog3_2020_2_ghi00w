var dir_a80c95aba07e0e6df592f1499cf5423d =
[
    [ "BookShelf.Data", "dir_94d4bd50f0f8dce3d8b8d0bdbfcaa9d2.html", "dir_94d4bd50f0f8dce3d8b8d0bdbfcaa9d2" ],
    [ "BookShelf.Logic", "dir_c4a2166bf39769ef75d7204b25acf79f.html", "dir_c4a2166bf39769ef75d7204b25acf79f" ],
    [ "BookShelf.Logic.Tests", "dir_967cc2fc8bede18258e22dd1468d8a7d.html", "dir_967cc2fc8bede18258e22dd1468d8a7d" ],
    [ "BookShelf.Program", "dir_c6bd9e6dc826b944efe813165a8696d6.html", "dir_c6bd9e6dc826b944efe813165a8696d6" ],
    [ "BookShelf.Repository", "dir_03ed961a1d24dbd15128737babdc0d5f.html", "dir_03ed961a1d24dbd15128737babdc0d5f" ]
];