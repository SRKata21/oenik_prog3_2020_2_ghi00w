﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Abstract repository class for implementing CR(U)D methodes.
    /// </summary>
    /// <typeparam name="T">T is a generic type for entities.</typeparam>
    public abstract class Manager<T> : IManager<T>
        where T : class
    {
        /// <summary>
        /// dbctx is the Database reference.
        /// </summary>
        private DbContext dbctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Manager{T}"/> class.
        /// </summary>
        /// <param name="ctx">The given DbContext for the initialization.</param>
        public Manager(DbContext ctx)
        {
            this.dbctx = ctx;
        }

        /// <summary>
        /// It returns all existing objects with T type.
        /// </summary>
        /// <returns>A list of all existing objects. It's IQueriable so I can make queries using these.</returns>
        public IQueryable<T> GetAll()
        {
            return this.dbctx.Set<T>(); // Set makes connection with the entity sets in the database.
        }

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        public void Insert(T entity)
        {
            this.dbctx.Set<T>().Add(entity);
            this.dbctx.SaveChanges();
        }

        /// <summary>
        /// It removes the entity from the database.
        /// </summary>
        /// <param name="entity">The entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public bool Remove(T entity)
        {
            if (this.dbctx.Set<T>().Remove(entity) != null)
            {
                this.dbctx.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It removes the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public bool Remove(int id)
        {
            return this.Remove(GetOne(id));
        }
    }
}