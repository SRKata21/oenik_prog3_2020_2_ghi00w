﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// The descended interface of IRepository. The update is written here.
    /// </summary>
    public interface ILendManager : IManager<Lends>
    {
        /// <summary>
        /// It changes the bookid of the lending.
        /// </summary>
        /// <param name="id">The id of the lending.</param>
        /// <param name="newBookId">The new bookid of the lending.</param>
        public void ChangeBook(int id, int newBookId);

        /// <summary>
        /// It changes the personid of the lending.
        /// </summary>
        /// <param name="id">The bookid of the lending.</param>
        /// <param name="personId">The new personid of the lending.</param>
        public void ChangePerson(int id, int personId);

        /// <summary>
        /// It changes the date of the lending.
        /// </summary>
        /// <param name="id">The id of the lending.</param>
        /// <param name="ldate">The new date of the lending.</param>
        public void ChangeLendDate(int id, DateTime ldate);

        /// <summary>
        /// It changes the date of the giving back.
        /// </summary>
        /// <param name="id">The id of the lending.</param>
        /// <param name="gbdate">The new date of the giving back.</param>
        public void ChangeGiveBackDate(int id, DateTime gbdate);
    }
}
