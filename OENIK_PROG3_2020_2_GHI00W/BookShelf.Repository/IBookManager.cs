﻿namespace BookShelf.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// The descended interface of IRepository. The update is written here.
    /// </summary>
    public interface IBookManager : IManager<Books>
    {
        /// <summary>
        /// It changes the title of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="title">The new title of the book.</param>
        void ChangeTitle(int id, string title);

        /// <summary>
        /// It changes the authorid of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="authorId">The new authorid of the book.</param>
        void ChangeAuthor(int id, int authorId);

        /// <summary>
        /// It changes the publisher of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="publisher">The new publisher of the book.</param>
        void ChangePublisher(int id, string publisher);

        /// <summary>
        /// It changes the place where the book was published.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="place">The new place where the book was published.</param>
        void ChangePlace(int id, string place);

        /// <summary>
        /// It changes the year when the book was published.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="year">The new year when the book was published.</param>
        void ChangeYear(int id, int year);

        /// <summary>
        /// It changes the number of pages.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="page">The new page number of the book.</param>
        void ChangePage(int id, int page);

        /// <summary>
        /// It changes the date of getting the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="gdate">The new date of getting the book.</param>
        void ChangeGetDate(int id, DateTime gdate);
    }
}
