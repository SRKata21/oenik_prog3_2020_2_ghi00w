﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;

    /// <summary>
    /// This interface helps the reader in search.
    /// </summary>
    public class ReaderUser : IReaderUser
    {
        private IBookManager bookRepos;
        private IAuthorManager authorRepos;
        private IPeopleManager personRepos;
        private ILendManager lendRepos;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderUser"/> class.
        /// </summary>
        /// <param name="bookRepos">The book repository.</param>
        /// <param name="authorRepos">The author repository.</param>
        /// <param name="personRepos">The person repository.</param>
        /// <param name="lendRepos">The lending repository.</param>
        public ReaderUser(IBookManager bookRepos, IAuthorManager authorRepos, IPeopleManager personRepos, ILendManager lendRepos)
        {
            this.bookRepos = bookRepos;
            this.authorRepos = authorRepos;
            this.personRepos = personRepos;
            this.lendRepos = lendRepos;
        }

        /// <summary>
        /// It changes the address of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="address">The new the address of the person.</param>
        public void ChangePersonAddress(int id, string address)
        {
            this.personRepos.ChangeAddress(id, address);
        }

        /// <summary>
        /// It changes the email of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="email">The new the email of the person.</param>
        public void ChangePersonEmail(int id, string email)
        {
            this.personRepos.ChangeEmail(id, email);
        }

        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public IList<Authors> GetAllAuthors()
        {
            return this.authorRepos.GetAll().ToList<Authors>();
        }

        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        public IList<Books> GetAllBooks()
        {
            return this.bookRepos.GetAll().ToList<Books>();
        }

        /// <summary>
        /// It searches the books that are wrote by hungarian author.
        /// </summary>
        /// <returns>The list lendings of the reader.</returns>
        public IList<BookDatas> GetBookHungarian()
        {
            var datas = from book in this.bookRepos.GetAll()
                        join author in this.authorRepos.GetAll() on book.AuthorId equals author.AuthorId
                        where author.Nationality == "hungarian"
                        select new BookDatas()
                        {
                            Id = book.BookId,
                            Title = book.Title,
                            AuthorName = author.Name,
                        };

            return datas.ToList();
        }

        /// <summary>
        /// It searches the most popular books by lendings.
        /// </summary>
        /// <returns>The list lendings of the reader.</returns>
        public IList<BookDatas> TaskGetBookHungarian()
        {
            return Task<IList<BookDatas>>.Factory.StartNew(() => GetBookHungarian()).Result;
        }

        /// <summary>
        /// It searches the most popular books by lendings.
        /// </summary>
        /// <returns>The list lendings of the reader.</returns>
        public IList<BookDatas> GetFamousBooks()
        {
            var countL = from row in this.lendRepos.GetAll()
                           group row by row.BookId into bookGrp
                           orderby bookGrp.Count() descending
                           select new { id = bookGrp.Key, count= bookGrp.Count() };
            var mostL = from nrow in countL
                      where nrow.count == countL.Max(item => item.count)
                      select nrow.id;
            List<int> mostList = mostL.ToList();
            var mostFamous = (from lend in this.lendRepos.GetAll()
                         join book in this.bookRepos.GetAll() on lend.BookId equals book.BookId
                         join author in this.authorRepos.GetAll() on book.AuthorId equals author.AuthorId
                         where mostList.Contains(book.BookId)
                         select new BookDatas()
                         {
                             Id = book.BookId,
                             Title = book.Title,
                             AuthorName = author.Name,
                         }).Distinct();

            return mostFamous.ToList();
        }

        /// <summary>
        /// GetFamousBook in Task.
        /// </summary>
        /// <returns>It returns famous books in Task.</returns>
        public IList<BookDatas> TaskGetFamBook()
        {
            return Task<IList<BookDatas>>.Factory.StartNew(() => GetFamousBooks()).Result;
        }
    }
}
