﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// This interface helps the reader in search.
    /// </summary>
    public interface IReaderUser
    {
        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        IList<Books> GetAllBooks();

        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        IList<Authors> GetAllAuthors();

        /// <summary>
        /// It changes the address of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="address">The new the address of the person.</param>
        void ChangePersonAddress(int id, string address);

        /// <summary>
        /// It changes the email of the person.
        /// </summary>
        /// <param name="id">The id of the person.</param>
        /// <param name="email">The new the email of the person.</param>
        void ChangePersonEmail(int id, string email);

        /// <summary>
        /// It searches the most popular books by lendings.
        /// </summary>
        /// <returns>The list lendings of the reader.</returns>
        IList<BookDatas> GetFamousBooks();

        /// <summary>
        /// It searches the books that are wrote by hungarian author.
        /// </summary>
        /// <returns>The list lendings of the reader.</returns>
        IList<BookDatas> GetBookHungarian();
    }
}
