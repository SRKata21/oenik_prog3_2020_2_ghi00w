﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class of Logic which searches the most popular book by lendings.
    /// </summary>
    public class BookDatas
    {
        /// <summary>
        /// Gets or sets the id of the book.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title of the book.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the name of the author.
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// It tests whether the 2 objects are equal or not.
        /// </summary>
        /// <param name="obj">The object which is equal to this.</param>
        /// <returns>It returns true when the object eguals to this, else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is BookDatas)
            {
                BookDatas other = obj as BookDatas;
                return this.Id == other.Id;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It generates a hashcode for the object.
        /// </summary>
        /// <returns>It returns the hashcode.</returns>
        public override int GetHashCode()
        {
            return (this.Title.Length * this.Id / 3) + (this.AuthorName.Length / 11);
        }

        /// <summary>
        /// It returns the object created in string.
        /// </summary>
        /// <returns>The object like string.</returns>
        public override string ToString()
        {
            return $"({this.Id}) {this.Title} by {this.AuthorName}.";
        }
    }
}
