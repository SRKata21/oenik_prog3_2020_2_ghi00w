﻿namespace BookShelf.Logic
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Class of Logic which searches the dates of lendings by reader.
    /// </summary>
    public class LendingByReader
    {
        /// <summary>
        /// Gets or sets the title of the book.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the name of the author.
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// Gets or sets the date of lending.
        /// </summary>
        public DateTime LendDate { get; set; }

        /// <summary>
        /// Gets or sets the date of lending.
        /// </summary>
        public DateTime GiveBackDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the reader.
        /// </summary>
        public string ReaderName { get; set; }

        /// <summary>
        /// It tests whether the 2 objects are equal or not.
        /// </summary>
        /// <param name="obj">The object which is equal to this.</param>
        /// <returns>It returns true when the object eguals to this, else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is LendingByReader)
            {
                LendingByReader other = obj as LendingByReader;
                return this.Title == other.Title && this.ReaderName == other.ReaderName && this.LendDate == other.LendDate && this.AuthorName == other.AuthorName;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It generates a hashcode for the object.
        /// </summary>
        /// <returns>It returns the hashcode.</returns>
        public override int GetHashCode()
        {
            return (this.ReaderName.Length / 3) + this.LendDate.DayOfYear - this.AuthorName.Length;
        }

        /// <summary>
        /// It returns the object created in string.
        /// </summary>
        /// <returns>The object like string.</returns>
        public override string ToString()
        {
            if (this.GiveBackDate.Year == 0001)
            {
                return $"{this.ReaderName} lended the {this.Title} by {this.AuthorName} on {this.LendDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}.";
            }
            else
            {
                return $"{this.ReaderName} lended the {this.Title} by {this.AuthorName} on {this.LendDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))} and it was given back on {this.GiveBackDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}.";
            }
        }
    }
}
