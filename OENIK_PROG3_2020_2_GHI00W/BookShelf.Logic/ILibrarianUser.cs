﻿namespace BookShelf.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BookShelf.Data.Entities;

    /// <summary>
    /// This interface helps the librarian in search.
    /// </summary>
    public interface ILibrarianUser
    {
        /// <summary>
        /// It searches all entities by key from the database.
        /// </summary>
        /// <returns>Returns if the operating was succeded or not.</returns>
        IList<Lends> GetAllLends();

        /// <summary>
        /// It searches the dates of lendings by reader.
        /// </summary>
        /// <param name="idReader">It is the id of the reader.</param>
        /// <returns>The list lendings of the reader.</returns>
        IList<LendingByReader> GetLendByReader(int idReader);

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        Books GetOneBook(int id);

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        Authors GetOneAuthor(int id);

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        People GetOnePerson(int id);

        /// <summary>
        /// It searches the entity by key from the database.
        /// </summary>
        /// <param name="id">The int type key of the entity which should be removed from database.</param>
        /// <returns>Returns if the operating was succeded or not.</returns>
        Lends GetOneLend(int id);

        /// <summary>
        /// It makes the inserting in the tables of database.
        /// </summary>
        /// <param name="entity">The methode gets the entity to insert in the database.</param>
        void InsertLend(Lends entity);

        /// <summary>
        /// It changes the name of the author.
        /// </summary>
        /// <param name="id">The id of the author.</param>
        /// <param name="name">The new name of the author.</param>
        void ChangeAuthorName(int id, string name);

        /// <summary>
        /// It changes the title of the book.
        /// </summary>
        /// <param name="id">The id of the book.</param>
        /// <param name="y">The correct year of publishing the book.</param>
        void ChangeBookYear(int id, int y);

        /// <summary>
        /// It changes the date of the giving back.
        /// </summary>
        /// <param name="id">The bookid of the lending.</param>
        /// <param name="gbdate">The new date of the giving back.</param>
        void ChangeLendGiveBackDate(int id, DateTime gbdate);
    }
}
