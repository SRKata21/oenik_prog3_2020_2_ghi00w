﻿namespace BookShelf.Program
{
    using System;
    using System.Globalization;
    using BookShelf.Data.Entities;
    using BookShelf.Logic;
    using BookShelf.Repository;
    using ConsoleTools;

    /// <summary>
    /// Main class, Program in .Program.
    /// </summary>
    internal class ConsoleCommunicator
    {
        /// <summary>
        /// The Meniu program.
        /// </summary>
        private static void Main()
        {
            Factory fact = new Factory(new BookShelfDBContext());

            ConsoleMenu mainMenu = new ConsoleMenu();
            ConsoleMenu readerMenu = new ConsoleMenu();
            ConsoleMenu librarianMenu = new ConsoleMenu();
            ConsoleMenu administratorMenu = new ConsoleMenu();
            ConsoleMenu bookMenu = new ConsoleMenu();
            ConsoleMenu authorMenu = new ConsoleMenu();
            ConsoleMenu peopleMenu = new ConsoleMenu();
            ConsoleMenu lendsMenu = new ConsoleMenu();

            mainMenu.Add("I am an administrator", () => administratorMenu.Show()).
                Add("I am a librarian", () => librarianMenu.Show()).
                Add("I am a reader", () => readerMenu.Show()).
                Add("Close", ConsoleMenu.Close);

            librarianMenu.Add("Enumerate all lendings", () => GetAllLend(fact.Lbr)).
                Add("Get lendings of a person", () => GetLendByReader(fact.Lbr)).
                Add("Get lendings of a person (task)", () => GetLendByReaderAsync(fact.Lbr)).
                Add("Get one lend by id", () => GetOneLend(fact.Lbr)).
                Add("Insert new lend", () => InsertLend(fact.Lbr)).
                Add("Change the date of giving back a book", () => ChangeGiveBackDate(fact.Lbr)).
                Add("Get an author by id", () => GetOneAuthor(fact.Lbr)).
                Add("Change the name of the author", () => ChangeAuthorName(fact.Lbr)).
                Add("Get one book by id", () => GetOneBook(fact.Lbr)).
                Add("Change the year of the publishing", () => ChangeBookYear(fact.Lbr)).
                Add("Get a person by id", () => GetOneReader(fact.Lbr)).
                Add("Close", ConsoleMenu.Close);

            readerMenu.Add("Enumerate all authors", () => GetAllAuthor(fact.Rdr)).
                Add("Enumerate all books", () => GetAllBook(fact.Rdr)).
                Add("Enumerate all books wrote by hungarian author", () => GetAllHungarianBook(fact.Rdr)).
                Add("Enumerate all books wrote by hungarian author (task)", () => GetAllHungarianBookAsync(fact.Rdr)).
                Add("Enumerate the most popular books", () => GetPopularBook(fact.Rdr)).
                Add("Enumerate the most popular books (task)", () => GetPopularBookAsync(fact.Rdr)).
                Add("Change address", () => ChangeAddress(fact.Rdr)).
                Add("Change e-mail address", () => ChangeEmail(fact.Rdr)).
                Add("Close", ConsoleMenu.Close);

            administratorMenu.Add("Enumerate all people", () => GetAllPeople(fact.Adm)).
                Add("Get one person by id", () => GetOnePerson(fact.Adm)).
                Add("Insert a new reader", () => InsertPerson(fact.Adm)).
                Add("Delete a reader from the database", () => RemovePerson(fact.Adm)).
                Add("Insert a new author", () => InsertAuthor(fact.Adm)).
                Add("Delete an author from the database", () => RemoveAuthor(fact.Adm)).
                Add("Insert a new book", () => InsertBook(fact.Adm)).
                Add("Delete a book from the database", () => RemoveBook(fact.Adm)).
                Add("Close", ConsoleMenu.Close);

            mainMenu.Show();
        }

        private static void GetAllAuthor(ReaderUser readr)
        {
            foreach (var item in readr.GetAllAuthors())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetAllBook(ReaderUser readr)
        {
            foreach (var item in readr.GetAllBooks())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void Message(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
        }

        private static void Message2(string message)
        {
            Console.WriteLine(message);
        }

        private static void GetAllLend(LibrarianUser libr)
        {
            foreach (var item in libr.GetAllLends())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetLendByReader(LibrarianUser libr)
        {
            Message2("Write the id of the wanted person!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            foreach (var item in libr.GetLendByReader(id))
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetLendByReaderAsync(LibrarianUser libr)
        {
            Message2("Write the id of the wanted person!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            foreach (var item in libr.TaskGetLendByReader(id))
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void ChangeGiveBackDate(LibrarianUser libr)
        {
            Message2("Write the id of the wanted lending!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            libr.ChangeLendGiveBackDate(id, DateTime.Now);

            Message("Change success.\nPress the enter!");
        }

        private static void GetOneAuthor(LibrarianUser libr)
        {
            Message2("Write the id of the wanted author!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Console.WriteLine(libr.GetOneAuthor(id));
            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void InsertAuthor(AdministratorUser admin)
        {
            Authors authr = new Authors();
            Message2("Write the writer name of the new author!");
            authr.Name = Console.ReadLine();
            Message2("Write the real name of the new author!");
            authr.BirthName = Console.ReadLine();
            Message2("Write the country where the new author was born!");
            authr.BirthCountry = Console.ReadLine();
            Message2("Write the year when the new author was born!");
            authr.BirthYear = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the nationality of the new Author!");
            authr.Nationality = Console.ReadLine();
            admin.InsertAuthor(authr);
            Message("Insert success\nPress the enter!");
        }

        private static void ChangeAuthorName(LibrarianUser libr)
        {
            Message2("Write the id of the wanted author!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the correct writer name of the author!");
            string name = Console.ReadLine();
            libr.ChangeAuthorName(id, name);
            Message("Change success\nPress the enter!");
        }

        private static void GetOneBook(LibrarianUser libr)
        {
            Message2("Write the id of the wanted book!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Console.WriteLine(libr.GetOneBook(id));
            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void ChangeBookYear(LibrarianUser libr)
        {
            Message2("Write the id of the wanted book!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the correct year of the wanted book!");
            int year = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            libr.ChangeBookYear(id, year);

            Message("Change success\nPress the enter!");
        }

        private static void GetOneReader(LibrarianUser libr)
        {
            Message2("Write the id of the wanted reader!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Console.WriteLine(libr.GetOnePerson(id));
            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetOneLend(LibrarianUser libr)
        {
            Message2("Write the id of the wanted lending!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Console.WriteLine(libr.GetOneLend(id));
            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void InsertLend(LibrarianUser libr)
        {
            Lends lend = new Lends();
            Message2("Write the id of the reader!");
            lend.PersonId = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the id of the book!");
            lend.BookId = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            lend.LendDate = DateTime.Now;
            libr.InsertLend(lend);
            Message("Insert success\nPress the enter!");
        }

        private static void GetAllHungarianBook(ReaderUser readr)
        {
            foreach (var item in readr.GetBookHungarian())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetAllHungarianBookAsync(ReaderUser readr)
        {
            foreach (var item in readr.TaskGetBookHungarian())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetPopularBook(ReaderUser readr)
        {
            foreach (var item in readr.GetFamousBooks())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetPopularBookAsync(ReaderUser readr)
        {
            foreach (var item in readr.TaskGetFamBook())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void ChangeAddress(ReaderUser readr)
        {
            Message2("Write your id!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write your new address!");
            string address = Console.ReadLine();
            readr.ChangePersonAddress(id, address);

            Message("Change success\nPress the enter!");
        }

        private static void ChangeEmail(ReaderUser readr)
        {
            Message2("Write your id!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write your new e-mail!");
            string email = Console.ReadLine();
            readr.ChangePersonEmail(id, email);

            Message("Change success\nPress the enter!");
        }

        private static void GetAllPeople(AdministratorUser admin)
        {
            foreach (var item in admin.GetAllPeople())
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void GetOnePerson(AdministratorUser admin)
        {
            Message2("Write the id of the wanted reader!");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Console.WriteLine(admin.GetOnePerson(id));
            Console.WriteLine();
            Message("Press the enter!");
        }

        private static void InsertPerson(AdministratorUser admin)
        {
            People person = new People();
            Message2("Write the name of the new reader!");
            person.Name = Console.ReadLine();
            Message2("Write the name of the mother of the new reader!");
            person.MotherName = Console.ReadLine();
            Message2("Write the address of the new reader!");
            person.Address = Console.ReadLine();
            Message2("Write the year when the new reader was born!");
            int year = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the month when the new reader was born!");
            int month = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the day when the new reader was born!");
            int day = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            person.BirthDate = Convert.ToDateTime(year + "." + month + "." + day, CultureInfo.CurrentCulture);
            Message2("Write the place where the person was born!");
            person.BirthPlace = Console.ReadLine();
            Message2("Write the email of new reader!");
            person.Email = Console.ReadLine();
            person.EnterDate = DateTime.Now;
            admin.InsertPerson(person);
            Message("Insert success\nPress the enter!");
        }

        private static void RemovePerson(AdministratorUser admin)
        {
            Message2("Write the id of the removable reader.");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            if (admin.RemovePersonByKey(id))
            {
                Message("Delete success\nPress the enter!");
            }
            else
            {
                Message("Delete failed\nPress the enter!");
            }
        }

        private static void InsertBook(AdministratorUser admin)
        {
            Books book = new Books();
            Message2("Write the title of the new book!");
            book.Title = Console.ReadLine();
            Message2("Write the id of the author!");
            book.AuthorId = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the publisher of the new book!");
            book.Publisher = Console.ReadLine();
            Message2("Write the place where the new book was published!");
            book.Place = Console.ReadLine();
            Message2("Write the year when the new book was published!");
            book.Year = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            Message2("Write the page numbers of new book!");
            book.Pages = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            book.GetDate = DateTime.Now;
            admin.InsertBook(book);
            Message("Insert success\nPress the enter!");
        }

        private static void RemoveBook(AdministratorUser admin)
        {
            Message2("Write the id of the removable book.");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            if (admin.RemoveBookByKey(id))
            {
                Message("Delete success\nPress the enter!");
            }
            else
            {
                Message("Delete failed\nPress the enter!");
            }
        }

        private static void RemoveAuthor(AdministratorUser admin)
        {
            Message2("Write the id of the removable author.");
            int id = int.Parse(Console.ReadLine(), new CultureInfo("en-US"));
            if (admin.RemoveAuthorByKey(id))
            {
                Message("Delete success\nPress the enter!");
            }
            else
            {
                Message("Delete failed\nPress the enter!");
            }
        }
    }
}
