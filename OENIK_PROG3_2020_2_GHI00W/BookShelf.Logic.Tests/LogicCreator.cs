﻿namespace BookShelf.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;
    using Moq;

    /// <summary>
    /// Creates the Loqic for tests.
    /// </summary>
    internal static class LogicCreator
    {
        /// <summary>
        /// Gets or Sets the mock repository for books.
        /// </summary>
        public static Mock<IBookManager> MockBookRepo { get; set; }

        /// <summary>
        /// Gets or Sets the mock repository for authors.
        /// </summary>
        public static Mock<IAuthorManager> MockAuthorRepo { get; set; }

        /// <summary>
        /// Gets or Sets the mock repository for people.
        /// </summary>
        public static Mock<IPeopleManager> MockPeopleRepo { get; set; }

        /// <summary>
        /// Gets or Sets the mock repository for lendings.
        /// </summary>
        public static Mock<ILendManager> MockLendRepo { get; set; }

        /// <summary>
        /// Gets or sets the expected result for the GetLendingByReader.
        /// </summary>
        public static List<LendingByReader> ExpectedLends { get; set; }

        /// <summary>
        /// Gets or sets the expected result for the GetFamousBooks.
        /// </summary>
        public static List<BookDatas> ExpectedFamBooks { get; set; }

        /// <summary>
        /// Gets or sets the expected result for the GetBookHungarian.
        /// </summary>
        public static List<BookDatas> ExpectedHunBooks { get; set; }

        /// <summary>
        /// Creates the needed logic for non-crud tests.
        /// </summary>
        /// <returns> The readerUser logic.</returns>
        public static ReaderUser CreateReaderLogicWithMocks()
        {
            MockBookRepo = new Mock<IBookManager>(MockBehavior.Loose);
            MockAuthorRepo = new Mock<IAuthorManager>(MockBehavior.Loose);
            MockPeopleRepo = new Mock<IPeopleManager>(MockBehavior.Loose);
            MockLendRepo = new Mock<ILendManager>(MockBehavior.Loose);

            List<Books> books = new List<Books>
            {
                new Books() { BookId = 1, AuthorId = 2, Title = "Macska a galambok között" },
                new Books() { BookId = 2, AuthorId = 2, Title = "A nagy négyes" },
                new Books() { BookId = 3, AuthorId = 3, Title = "Átmeneti üresedés" },
                new Books() { BookId = 4, AuthorId = 3, Title = "Harry Potter és a tűz serlege" },
                new Books() { BookId = 5, AuthorId = 4, Title = "Vuk" },
                new Books() { BookId = 6, AuthorId = 5, Title = "Rejtőző kavicsok I" },
                new Books() { BookId = 7, AuthorId = 4, Title = "Bogáncs" },
            };
            List<Authors> authors = new List<Authors>
            {
                new Authors() { AuthorId = 1, Name = "Unknown", Nationality = "-" },
                new Authors() { AuthorId = 2, Name = "Agatha Christie", Nationality = "british" },
                new Authors() { AuthorId = 3, Name = "J. K. Rowling", Nationality = "british" },
                new Authors() { AuthorId = 4, Name = "Fekete István", Nationality = "hungarian" },
                new Authors() { AuthorId = 5, Name = "L. I. Lázár", Nationality = "hungarian" },
            };
            List<Lends> lend = new List<Lends>
            {
                new Lends() { LendId = 1, PersonId = 1, BookId = 1, LendDate = Convert.ToDateTime("2016.03.16", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2016.04.10", CultureInfo.CurrentCulture) },
                new Lends() { LendId = 2, PersonId = 1, BookId = 3, LendDate = Convert.ToDateTime("2017.06.06", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2017.07.01", CultureInfo.CurrentCulture) },
                new Lends() { LendId = 3, PersonId = 2, BookId = 2, LendDate = Convert.ToDateTime("2017.01.12", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2017.01.22", CultureInfo.CurrentCulture) },
                new Lends() { LendId = 4, PersonId = 2, BookId = 4, LendDate = Convert.ToDateTime("2019.12.05", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2019.12.15", CultureInfo.CurrentCulture) },
                new Lends() { LendId = 5, PersonId = 3, BookId = 5, LendDate = Convert.ToDateTime("2020.10.29", CultureInfo.CurrentCulture) },
                new Lends() { LendId = 6, PersonId = 1, BookId = 2, LendDate = Convert.ToDateTime("2018.01.12", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2018.01.22", CultureInfo.CurrentCulture) },
                new Lends() { LendId = 7, PersonId = 2, BookId = 4, LendDate = Convert.ToDateTime("2020.11.05", CultureInfo.CurrentCulture) },
            };

            ExpectedFamBooks = new List<BookDatas>
            {
                new BookDatas() { Id = 2, AuthorName = "Agatha Christie", Title = "A nagy négyes" },
                new BookDatas() { Id = 4, AuthorName = "J. K. Rowling", Title = "Harry Potter és a tűz serlege" },
            };
            ExpectedHunBooks = new List<BookDatas>
            {
                new BookDatas() { Id = 5, AuthorName = "Fekete István", Title = "Vuk" },
                new BookDatas() { Id = 6, AuthorName = "L. I. Lázár", Title = "Rejtőző kavicsok I" },
                new BookDatas() { Id = 7, AuthorName = "Fekete István", Title = "Bogáncs" },
            };

            MockAuthorRepo.Setup(repo => repo.GetAll()).Returns(authors.AsQueryable());
            MockBookRepo.Setup(repo => repo.GetAll()).Returns(books.AsQueryable());
            MockLendRepo.Setup(repo => repo.GetAll()).Returns(lend.AsQueryable());

            return new ReaderUser(MockBookRepo.Object, MockAuthorRepo.Object, MockPeopleRepo.Object, MockLendRepo.Object);
        }

        /// <summary>
        /// Creates the needed logic for non-crud tests.
        /// </summary>
        /// <returns> The librarianUser logic.</returns>
        public static LibrarianUser CreateLibrarianLogicWithMocks()
        {
            MockBookRepo = new Mock<IBookManager>(MockBehavior.Loose);
            MockAuthorRepo = new Mock<IAuthorManager>(MockBehavior.Loose);
            MockPeopleRepo = new Mock<IPeopleManager>(MockBehavior.Loose);
            MockLendRepo = new Mock<ILendManager>(MockBehavior.Loose);

            List<People> person = new List<People>
            {
                new People() { PersonId = 1, Name = "Nemecsek Ernő" },
                new People() { PersonId = 2, Name = "Nyilas Misi" },
                new People() { PersonId = 3, Name = "Lúdas Matyi" },
            };
            List<Books> books = new List<Books>
            {
                new Books() { BookId = 1, AuthorId = 2, Title = "Macska a galambok között" },
                new Books() { BookId = 2, AuthorId = 2, Title = "A nagy négyes" },
                new Books() { BookId = 3, AuthorId = 3, Title = "Átmeneti üresedés" },
                new Books() { BookId = 4, AuthorId = 3, Title = "Harry Potter és a tűz serlege" },
                new Books() { BookId = 5, AuthorId = 4, Title = "Vuk" },
                new Books() { BookId = 6, AuthorId = 5, Title = "Rejtőző kavicsok I" },
                new Books() { BookId = 7, AuthorId = 4, Title = "Bogáncs" },
            };
            List<Authors> authors = new List<Authors>
            {
                new Authors() { AuthorId = 1, Name = "Unknown", Nationality = "-" },
                new Authors() { AuthorId = 2, Name = "Agatha Christie", Nationality = "british" },
                new Authors() { AuthorId = 3, Name = "J. K. Rowling", Nationality = "british" },
                new Authors() { AuthorId = 4, Name = "Fekete István", Nationality = "hungarian" },
                new Authors() { AuthorId = 5, Name = "L. I. Lázár", Nationality = "hungarian" },
            };
            List<Lends> lend = new List<Lends>
            {
                new Lends() { PersonId = 1, BookId = 1, LendDate = Convert.ToDateTime("2016.03.16", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2016.04.10", CultureInfo.CurrentCulture) },
                new Lends() { PersonId = 1, BookId = 3, LendDate = Convert.ToDateTime("2017.06.06", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2017.07.01", CultureInfo.CurrentCulture) },
                new Lends() { PersonId = 2, BookId = 2, LendDate = Convert.ToDateTime("2017.01.12", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2017.01.22", CultureInfo.CurrentCulture) },
                new Lends() { PersonId = 2, BookId = 4, LendDate = Convert.ToDateTime("2019.12.05", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2019.12.15", CultureInfo.CurrentCulture) },
                new Lends() { PersonId = 3, BookId = 5, LendDate = Convert.ToDateTime("2020.10.29", CultureInfo.CurrentCulture) },
                new Lends() { PersonId = 1, BookId = 2, LendDate = Convert.ToDateTime("2018.01.12", CultureInfo.CurrentCulture), GivebackDate = Convert.ToDateTime("2018.01.22", CultureInfo.CurrentCulture) },
                new Lends() { PersonId = 2, BookId = 4, LendDate = Convert.ToDateTime("2020.11.05", CultureInfo.CurrentCulture) },
            };

            ExpectedLends = new List<LendingByReader>
            {
                new LendingByReader() { ReaderName = "Nemecsek Ernő", AuthorName = "Agatha Christie", Title = "Macska a galambok között", LendDate = Convert.ToDateTime("2016.03.16", CultureInfo.CurrentCulture), GiveBackDate = Convert.ToDateTime("2016.04.10", CultureInfo.CurrentCulture) },
                new LendingByReader() { ReaderName = "Nemecsek Ernő", AuthorName = "J. K. Rowling", Title = "Átmeneti üresedés", LendDate = Convert.ToDateTime("2017.06.06", CultureInfo.CurrentCulture), GiveBackDate = Convert.ToDateTime("2017.07.01", CultureInfo.CurrentCulture) },
                new LendingByReader() { ReaderName = "Nemecsek Ernő", AuthorName = "Agatha Christie", Title = "A nagy négyes", LendDate = Convert.ToDateTime("2018.01.12", CultureInfo.CurrentCulture), GiveBackDate = Convert.ToDateTime("2018.01.22", CultureInfo.CurrentCulture) },
            };

            MockAuthorRepo.Setup(repo => repo.GetAll()).Returns(authors.AsQueryable());
            MockBookRepo.Setup(repo => repo.GetAll()).Returns(books.AsQueryable());
            MockLendRepo.Setup(repo => repo.GetAll()).Returns(lend.AsQueryable());
            MockPeopleRepo.Setup(repo => repo.GetAll()).Returns(person.AsQueryable());

            return new LibrarianUser(MockBookRepo.Object, MockAuthorRepo.Object, MockPeopleRepo.Object, MockLendRepo.Object);
        }
    }
}
