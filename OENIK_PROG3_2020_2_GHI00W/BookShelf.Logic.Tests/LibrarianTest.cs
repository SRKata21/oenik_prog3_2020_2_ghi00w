﻿namespace BookShelf.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text;
    using BookShelf.Data.Entities;
    using BookShelf.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// LibrarianTest in .Logic.Tests. to check the LibrarianUser logic.
    /// </summary>
    [TestFixture]
    public class LibrarianTest
    {
        /// <summary>
        /// It tests wheather works correct or not the inserter in the author table.
        /// </summary>
        [Test]
        public void TestUpdateLendsGiveBackDate()
        {
            Mock<IBookManager> mockBookRepo = new Mock<IBookManager>(MockBehavior.Loose);
            Mock<IAuthorManager> mockAuthorRepo = new Mock<IAuthorManager>(MockBehavior.Loose);
            Mock<IPeopleManager> mockPeopleRepo = new Mock<IPeopleManager>(MockBehavior.Loose);
            Mock<ILendManager> mockLendRepo = new Mock<ILendManager>(MockBehavior.Loose);

            mockLendRepo.Setup(updater => updater.ChangeGiveBackDate(1, Convert.ToDateTime("2020.12.05", CultureInfo.CurrentCulture)));

            LibrarianUser libr = new LibrarianUser(mockBookRepo.Object, mockAuthorRepo.Object, mockPeopleRepo.Object, mockLendRepo.Object);
            libr.ChangeLendGiveBackDate(1, Convert.ToDateTime("2020.12.05", CultureInfo.CurrentCulture));

            mockLendRepo.Verify(updater => updater.ChangeGiveBackDate(1, Convert.ToDateTime("2020.12.05", CultureInfo.CurrentCulture)), Times.Once);
        }

        /// <summary>
        /// It tests wheather works correct or not the LendingByReader.
        /// </summary>
        [Test]
        public void TestGetLendByReader()
        {
            var logic = LogicCreator.CreateLibrarianLogicWithMocks();
            var understandableLends = logic.GetLendByReader(1);

            Assert.That(understandableLends, Is.EquivalentTo(LogicCreator.ExpectedLends));
            LogicCreator.MockPeopleRepo.Verify(peop => peop.GetAll(), Times.Once);
            LogicCreator.MockLendRepo.Verify(lnd => lnd.GetAll(), Times.Once);
            LogicCreator.MockBookRepo.Verify(bk => bk.GetAll(), Times.Once);
            LogicCreator.MockAuthorRepo.Verify(athr => athr.GetAll(), Times.Once);
        }
    }
}
