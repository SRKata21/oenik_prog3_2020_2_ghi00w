﻿namespace BookShelf.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// Lends class in the Data.Entity. It is a relation maker table.
    /// </summary>
    [Table("lends")]
    public class Lends
    {
        /// <summary>
        /// Gets or sets AuthorId is a primary key in the Authors table.
        /// </summary>
        [Key]
        public int LendId { get; set; }

        /// <summary>
        /// Gets or sets Pple is a reference for People which has lend a book.
        /// </summary>
        [ForeignKey(nameof(Pple))]
        [Required]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets Bok is a reference for Books which is lend.
        /// </summary>
        [ForeignKey(nameof(Bok))]
        public int BookId { get; set; }

        /// <summary>
        /// Gets or sets LendDate is an attribute in the Lends table. It signs the date of lending.
        /// </summary>
        [Required]
        public DateTime LendDate { get; set; }

        /// <summary>
        /// Gets or sets GivebackDate is an attribute in the Lends table. It signs the date of giving back.
        /// </summary>
        public DateTime GivebackDate { get; set; }

        /// <summary>
        /// Gets or sets Pple is a reference for People which has lend a book.
        /// </summary>
        [NotMapped]
        public virtual People Pple { get; set; }

        /// <summary>
        /// Gets or sets Bok is a reference for Books which is lend.
        /// </summary>
        [NotMapped]
        public virtual Books Bok { get; set; }

        /// <summary>
        /// It creates the string using datas of an author.
        /// </summary>
        /// <returns>It returns the created string.</returns>
        public override string ToString()
        {
            if (GivebackDate.Year == 0001)
            {
                return $"LendId: {LendId}. PersonId: {PersonId}. BookId: {BookId}. LendDate: {LendDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}.";
            }
            else
            {
                return $"LendId: {LendId}. PersonId: {PersonId}. BookId: {BookId}. LendDate: {LendDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}. GivebackDate: {GivebackDate.ToString("yyyy/MM/dd", new CultureInfo("en-US"))}.";
            }
        }

        /// <summary>
        /// It tests whether the 2 objects are equal or not.
        /// </summary>
        /// <param name="obj">The object which is equal to this.</param>
        /// <returns>It returns true when the object eguals to this, else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Lends)
            {
                Lends other = obj as Lends;
                return LendId == other.LendId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It generates a hashcode for the object.
        /// </summary>
        /// <returns>It returns the hashcode.</returns>
        public override int GetHashCode()
        {
            return ((BookId * 7) + (LendDate.DayOfYear / 29) - (PersonId * 13)) / 103;
        }
    }
}
