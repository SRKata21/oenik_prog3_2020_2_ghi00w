﻿namespace BookShelf.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Books class in the Data.Entities.
    /// </summary>
    [Table("books")]
    public class Books
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Books"/> class.
        /// </summary>
        public Books()
        {
            this.Lnd = new HashSet<Lends>();
        }

        /// <summary>
        /// Gets or sets BookId is a primary key in the Books table.
        /// </summary>
        [Key]
        public int BookId { get; set; }

        /// <summary>
        /// Gets or sets Title is an attribute key in the Books table.
        /// </summary>
        [MaxLength(150)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets Publisher is an attribute in the Books table.
        /// </summary>
        [MaxLength(110)]
        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets Place is an attribute in the Books table. The place where the book was published.
        /// </summary>
        [MaxLength(115)]
        public string Place { get; set; }

        /// <summary>
        /// Gets or sets Year is an attribute in the Books table. The year when the book was published.
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Gets or sets Pages is an attribute in the Books table.
        /// </summary>
        public int Pages { get; set; }

        /// <summary>
        /// Gets or sets LendDate is an attribute in the Books table. It signs the date of getting.
        /// </summary>
        public DateTime GetDate { get; set; }

        /// <summary>
        /// Gets or sets Pple is a reference for People which has lend a book.
        /// </summary>
        [ForeignKey(nameof(Authr))]
        public int AuthorId { get; set; }

        /// <summary>
        /// Gets or sets Authr is a reference for Authors which has written the actual book.
        /// </summary>
        [NotMapped]
        public virtual Authors Authr { get; set;  }

        /// <summary>
        /// Gets LbryN is a reference collection for notes which show us the date of lend.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Lends> Lnd { get; }

        /// <summary>
        /// It creates the string using datas of an author.
        /// </summary>
        /// <returns>It returns the created string.</returns>
        public override string ToString()
        {
            return $"{BookId}. {Title} was published at {Publisher} ({Place}) in {Year} (Pages: {Pages})";
        }

        /// <summary>
        /// It tests whether the 2 objects are equal or not.
        /// </summary>
        /// <param name="obj">The object which is equal to this.</param>
        /// <returns>It returns true when the object eguals to this, else false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Books)
            {
                Books other = obj as Books;
                return BookId == other.BookId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// It generates a hashcode for the object.
        /// </summary>
        /// <returns>It returns the hashcode.</returns>
        public override int GetHashCode()
        {
            return ((Pages / 7) + (Year * 3) - (Title.Length * 13 / 3)) / 107;
        }
    }
}
